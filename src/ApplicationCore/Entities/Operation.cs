﻿using Ardalis.GuardClauses;
using Microsoft.MyMoney.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.ApplicationCore.Entities
{
    public class Operation : BaseEntity, IAggregateRoot
    {
        public int OperationTypeId { get; set; }

        public OperationType OperationType { get; set; }

        public int BankAccountId { get; set; }

        public BankAccount BankAccount { get; set; }

        public decimal Value { get; set; }

        public Operation(int operationTypeId, int bankAccountId, decimal value)
        {
            Guard.Against.Null(operationTypeId, nameof(operationTypeId));
            Guard.Against.Null(bankAccountId, nameof(bankAccountId));
            Guard.Against.Null(value, nameof(value));

            OperationTypeId = operationTypeId;
            BankAccountId = bankAccountId;
            Value = value; 
        }
    }
}
