﻿using Ardalis.GuardClauses;
using Microsoft.MyMoney.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.ApplicationCore.Entities
{
    public class BankAccount : BaseEntity, IAggregateRoot
    {
        public string OwnerId { get; private set; }

        public decimal Balance { get; private set; }

        public BankAccount()
        {
        }

        public BankAccount(string ownerId)
            :this(ownerId, 0)
        {           
        }

        public BankAccount(string ownerId, decimal balance)
        {
            Guard.Against.NullOrEmpty(ownerId, nameof(ownerId));
            Guard.Against.Null(balance, nameof(balance));

            OwnerId = ownerId;
            Balance = balance;
        }

    }
}
