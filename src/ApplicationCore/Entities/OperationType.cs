﻿using Ardalis.GuardClauses;
using Microsoft.MyMoney.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.ApplicationCore.Entities
{
    public class OperationType : BaseEntity, IAggregateRoot
    {
        public string Description { get; set; }

        public OperationType(string description)
        {
            Guard.Against.NullOrEmpty(description, nameof(description));

            Description = description;
        }
    }
}
