﻿using Microsoft.MyMoney.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.ApplicationCore.Entities
{
    public class Transfer : BaseEntity, IAggregateRoot
    {
        public int OriginOperationId { get; set; }

        public Operation OriginOperation { get; set; }

        public int TargetOperationId { get; set; }

        public Operation TargetOperation { get; set; }
    }
}
