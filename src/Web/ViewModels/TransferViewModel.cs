﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.ViewModels
{
    public class TransferViewModel
    {
        public int OriginOperationId { get; set; }

        public int TargerOperationId { get; set; }
    }
}
