﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.ViewModels
{
    public class OperationViewModel
    {
        public int OperationTypeId { get; set; }

        public int BankAccountId { get; set; }

        public decimal Value { get; set; }
    }
}
