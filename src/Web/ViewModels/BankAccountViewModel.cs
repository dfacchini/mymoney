﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.ViewModels
{
    public class BankAccountViewModel
    {
        public int Id { get; set; }

        public string OwnerId { get; set; }

        public decimal Balance { get; set; }
    }
}
