﻿namespace Microsoft.MyMoney.Web.ViewModels.Manage
{
    public class GenerateRecoveryCodesViewModel
    {
        public string[] RecoveryCodes { get; set; }
    }
}
