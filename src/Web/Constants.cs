﻿namespace Microsoft.MyMoney.Web
{
    public static class Constants
    {
        public const string BASKET_COOKIENAME = "MyMoney";
        public const int ITEMS_PER_PAGE = 10;
        public const string DEFAULT_USERNAME = "Guest";
    }
}
