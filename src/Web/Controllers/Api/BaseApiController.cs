﻿using Microsoft.AspNetCore.Mvc;

namespace Microsoft.MyMoney.Web.Controllers.Api
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    { }
}
