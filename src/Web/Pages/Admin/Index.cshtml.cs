﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.MyMoney.ApplicationCore.Constants;
using Microsoft.MyMoney.Web.Extensions;
using Microsoft.MyMoney.Web.Services;
using Microsoft.MyMoney.Web.ViewModels;
using Microsoft.Extensions.Caching.Memory;
using System.Threading.Tasks;
using Microsoft.MyMoney.Web.Interfaces;

namespace Microsoft.MyMoney.Web.Pages.Admin
{
    [Authorize(Roles = AuthorizationConstants.Roles.ADMINISTRATORS)]
    public class IndexModel : PageModel
    {
        private readonly IBankAccountViewModelService _bankAccountViewModelService;
        private readonly IMemoryCache _cache;

        public IndexModel(IBankAccountViewModelService bankAccountViewModelService, IMemoryCache cache)
        {
            _bankAccountViewModelService = bankAccountViewModelService;
            _cache = cache;
        }

        public BankAccountViewModel BankAccountViewModel { get; set; } = new BankAccountViewModel();

        public async Task OnGet(BankAccountViewModel bankAccountViewModel, int id)
        {
            var cacheKey = CacheHelpers.GenerateBankAccountCacheKey(id);
            
            _cache.Remove(cacheKey);

            BankAccountViewModel = await _bankAccountViewModelService.GetBankAccount(bankAccountViewModel.Id);
        }
    }
}
