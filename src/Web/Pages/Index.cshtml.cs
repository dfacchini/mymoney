﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.MyMoney.Web.Interfaces;
using Microsoft.MyMoney.Web.Services;
using Microsoft.MyMoney.Web.ViewModels;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IBankAccountViewModelService _bankAccountViewModelService;

        public IndexModel(IBankAccountViewModelService bankAccountViewModelService)
        {
            _bankAccountViewModelService = bankAccountViewModelService;
        }

        public BankAccountViewModel BankModel { get; set; } = new BankAccountViewModel();

        public async Task OnGet(BankAccountViewModel bankAccountModel)
        {
            BankModel = await _bankAccountViewModelService.GetBankAccount(bankAccountModel.Id);
        }
    }
}
