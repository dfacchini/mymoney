﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.MyMoney.Web.Extensions;
using Microsoft.MyMoney.Web.Interfaces;
using Microsoft.MyMoney.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.Services
{
    public class CachedBankAccountViewModelService : IBankAccountViewModelService
    {
        private readonly IMemoryCache _cache;
        private readonly BankAccountViewModelService _bankAccountViewModelService;

        public CachedBankAccountViewModelService(IMemoryCache cache,
            BankAccountViewModelService bankAccountViewModelService)
        {
            _cache = cache;
            _bankAccountViewModelService = bankAccountViewModelService;
        }

        public async Task<BankAccountViewModel> GetBankAccount(int id)
        {
            var cacheKey = CacheHelpers.GenerateBankAccountCacheKey(id);

            return await _cache.GetOrCreateAsync(cacheKey, async entry =>
            {
                entry.SlidingExpiration = CacheHelpers.DefaultCacheDuration;
                return await _bankAccountViewModelService.GetBankAccount(id);
            });
        }
    }
}
