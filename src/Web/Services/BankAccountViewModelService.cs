﻿using Microsoft.Extensions.Logging;
using Microsoft.MyMoney.ApplicationCore.Entities;
using Microsoft.MyMoney.ApplicationCore.Interfaces;
using Microsoft.MyMoney.Web.Interfaces;
using Microsoft.MyMoney.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.Services
{
    public class BankAccountViewModelService : IBankAccountViewModelService
    {

        private readonly ILogger<BankAccountViewModelService> _logger;
        private readonly IAsyncRepository<BankAccount> _bankAccountRepository;        

        public BankAccountViewModelService(
            ILoggerFactory loggerFactory,
            IAsyncRepository<BankAccount> bankAccountRepository)           
        {
            _logger = loggerFactory.CreateLogger<BankAccountViewModelService>();
            _bankAccountRepository = bankAccountRepository;            
        }

        public async Task<BankAccountViewModel> GetBankAccount(int id)
        {
            var bankAccount = await _bankAccountRepository.GetByIdAsync(id);
            if (bankAccount != null)
                return new BankAccountViewModel() { Id = bankAccount.Id, OwnerId = bankAccount.OwnerId, Balance = bankAccount.Balance };
            else
                return new BankAccountViewModel();
        }
    }
}