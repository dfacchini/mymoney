﻿using System;

namespace Microsoft.MyMoney.Web.Extensions
{
    public static class CacheHelpers
    {
        public static readonly TimeSpan DefaultCacheDuration = TimeSpan.FromSeconds(30);
        private static readonly string _bankAccountKeyTemplate = "bank_account-{0}";

        public static string GenerateBankAccountCacheKey(int id)
        {
            return string.Format(_bankAccountKeyTemplate, id);
        } 
    }
}
