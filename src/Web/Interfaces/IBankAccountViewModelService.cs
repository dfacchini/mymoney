﻿using Microsoft.MyMoney.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.MyMoney.Web.Interfaces
{
    public interface IBankAccountViewModelService
    {
        Task<BankAccountViewModel> GetBankAccount(int id);
        //Task<IEnumerable<SelectListItem>> GetOperations();
        //Task<IEnumerable<SelectListItem>> GetTransfers();
    }
}
