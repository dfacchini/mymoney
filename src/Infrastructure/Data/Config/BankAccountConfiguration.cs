﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.MyMoney.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.Infrastructure.Data.Config
{
    public class BankAccountConfiguration : IEntityTypeConfiguration<BankAccount>
    {
        public void Configure(EntityTypeBuilder<BankAccount> builder)
        {
            builder.ToTable("bank_account");

            builder.Property(ci => ci.Id)
                .UseHiLo("bank_account_hilo")
                .IsRequired();

            builder.Property(ci => ci.OwnerId)
                .IsRequired(true);

            builder.Property(ci => ci.Balance)
                .IsRequired(true)
                .HasColumnType("decimal(18,2)");
        }
    }
}
