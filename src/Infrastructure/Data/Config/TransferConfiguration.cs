﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.MyMoney.ApplicationCore.Entities;


namespace Microsoft.MyMoney.Infrastructure.Data.Config
{
    public class TransferConfiguration : IEntityTypeConfiguration<Transfer>
    {
        public void Configure(EntityTypeBuilder<Transfer> builder)
        {             
            builder.ToTable("transfer");

            builder.Property(ci => ci.Id)
                .UseHiLo("transfer_hilo")
                .IsRequired();

            builder.Property(ci => ci.TargetOperationId)
                .IsRequired(true);

            builder.Property(ci => ci.OriginOperationId)
                .IsRequired(true);

            builder.HasOne(ci => ci.TargetOperation)
                .WithMany()
                .HasForeignKey(ci => ci.TargetOperationId).OnDelete(DeleteBehavior.Restrict);
                

            builder.HasOne(ci => ci.OriginOperation)
                .WithMany()
                .HasForeignKey(ci => ci.OriginOperationId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
