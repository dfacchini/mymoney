﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.MyMoney.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.Infrastructure.Data.Config
{
    public class OperationTypeConfiguration : IEntityTypeConfiguration<OperationType>
    {
        public void Configure(EntityTypeBuilder<OperationType> builder)
        {
            builder.ToTable("operation_type");

            builder.Property(ci => ci.Id)
                .UseHiLo("operation_type_hilo")
                .IsRequired();

            builder.Property(ci => ci.Description)
                    .IsRequired(true);
        }
    }
}
