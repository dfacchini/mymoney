﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.MyMoney.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.MyMoney.Infrastructure.Data.Config
{
    class OperationConfiguration : IEntityTypeConfiguration<Operation>
    {
        public void Configure(EntityTypeBuilder<Operation> builder)
        {
            builder.ToTable("operation");

            builder.HasKey(ci => ci.Id);

            builder.Property(ci => ci.Id)
                .UseHiLo("operation_hilo")
                .IsRequired();

            builder.Property(ci => ci.OperationTypeId)
                .IsRequired(true);

            builder.Property(ci => ci.BankAccountId)
                .IsRequired(true);

            builder.Property(ci => ci.Value)
                .IsRequired(true)
                .HasColumnType("decimal(18,2)");


            builder.HasOne(ci => ci.OperationType)
                .WithMany()
                .HasForeignKey(ci => ci.OperationTypeId);

            builder.HasOne(ci => ci.BankAccount)
                .WithMany()
                .HasForeignKey(ci => ci.BankAccountId);
        }
    }
}
