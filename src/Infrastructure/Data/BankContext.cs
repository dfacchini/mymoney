﻿using Microsoft.EntityFrameworkCore;
using Microsoft.MyMoney.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Microsoft.MyMoney.Infrastructure.Data
{
    public class BankContext : DbContext
    {
        public BankContext(DbContextOptions<BankContext> options) : base(options)
        {

        }

        public DbSet<BankAccount> Accounts { get; set; }

        public DbSet<OperationType> OperationTypes { get; set; }

        public DbSet<Operation> Operations { get; set; }

        public DbSet<Transfer> Transfers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {            
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
