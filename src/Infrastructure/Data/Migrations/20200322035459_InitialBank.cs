﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Microsoft.MyMoney.Infrastructure.Data.Migrations
{
    public partial class InitialBank : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "bank_account_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "operation_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "operation_type_hilo",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "transfer_hilo",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "bank_account",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    OwnerId = table.Column<string>(nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank_account", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "operation_type",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_operation_type", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "operation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    OperationTypeId = table.Column<int>(nullable: false),
                    BankAccountId = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_operation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_operation_bank_account_BankAccountId",
                        column: x => x.BankAccountId,
                        principalTable: "bank_account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_operation_operation_type_OperationTypeId",
                        column: x => x.OperationTypeId,
                        principalTable: "operation_type",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    OriginOperationId = table.Column<int>(nullable: false),
                    TargetOperationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_transfer_operation_OriginOperationId",
                        column: x => x.OriginOperationId,
                        principalTable: "operation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_transfer_operation_TargetOperationId",
                        column: x => x.TargetOperationId,
                        principalTable: "operation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_operation_BankAccountId",
                table: "operation",
                column: "BankAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_operation_OperationTypeId",
                table: "operation",
                column: "OperationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_OriginOperationId",
                table: "transfer",
                column: "OriginOperationId");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_TargetOperationId",
                table: "transfer",
                column: "TargetOperationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "transfer");

            migrationBuilder.DropTable(
                name: "operation");

            migrationBuilder.DropTable(
                name: "bank_account");

            migrationBuilder.DropTable(
                name: "operation_type");

            migrationBuilder.DropSequence(
                name: "bank_account_hilo");

            migrationBuilder.DropSequence(
                name: "operation_hilo");

            migrationBuilder.DropSequence(
                name: "operation_type_hilo");

            migrationBuilder.DropSequence(
                name: "transfer_hilo");
        }
    }
}
