﻿using Microsoft.AspNetCore.Identity;

namespace Microsoft.MyMoney.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
